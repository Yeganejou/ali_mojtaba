def func(a, b):
    return (a*b, a**b)


list = func(2, 3)

print(type(list))

list = [2, 3]
list2 = (2, 3)
list3 = {'ali': 20, 'mojtaba': 0}
list3['reza'] = -2

for key in list3:
    print(key, list3[key])

for value in list3:
    print(value)

list = [1, 2, 3]
list2 = list.copy()
print(list)
list.append(4)
print(list)
print(list2)

# def func(a, **args):
#     for i in args:
#         print(i, args[i])
#
# func(num1 = 20, num3 = 40, num80 = 90)

my_dict = {'ali': 20, 'mojtaba': 0}

for key, value in my_dict.items():
    print(key, value)


def func3(x, y):
    '''this function return x power y'''
    return (x ** y)


print(func3(2, 3))
print("func3 document:", func3.__doc__)

list = [2, 3, 8, -2, 12]
list.sort()
print(list)

list.remove(8)
print(list)

del list[0]
print(list)

import numpy as np
list = np.arange()

print(list[])
